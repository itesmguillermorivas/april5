﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonidito : MonoBehaviour
{
    public AudioSource player;
    public AudioClip[] sonidos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyUp(KeyCode.I))
        {
            player.clip = sonidos[0];
            player.Play();
        }
        else if (Input.GetKeyUp(KeyCode.O))
        {
            player.clip = sonidos[1];
            player.Play();
        }
        else if (Input.GetKeyUp(KeyCode.P)) {
            player.clip = sonidos[2];
            player.Play();
        }


    }
}
